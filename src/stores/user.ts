import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  const editedUser = ref<User>({ id: -1, login: "", name: "", password: "" });
  const isTable = ref(true);
  let lastID = 4;
  const dialog = ref(false);
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Administrator", password: "Pass@1412" },
    { id: 2, login: "user1", name: "User 1", password: "@1412" },
    { id: 3, login: "user2", name: "User 2", password: "@1412" },
  ]);
  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id == id);
    users.value.splice(index, 1);
  };
  const login = (loginName: string, password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if (index >= 0) {
      const user = users.value[index];
      if (user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  };
  const SaveUser = () => {
    if (editedUser.value.id < 0) {
      editedUser.value.id = lastID++;
      users.value.push(editedUser.value);
    } else {
      const index = users.value.findIndex(
        (item) => item.id === editedUser.value.id
      );
      users.value[index] = editedUser.value;
    }
    dialog.value = false;
    clear();
  };
  const editUser = (user: User) => {
    editedUser.value = { ...user };
    dialog.value = true;
  };
  const clear = () => {
    editedUser.value = { id: -1, login: "", name: "", password: "" };
  };
  return {
    isTable,
    users,
    deleteUser,
    dialog,
    editedUser,
    clear,
    SaveUser,
    editUser,
    login,
  };
});
