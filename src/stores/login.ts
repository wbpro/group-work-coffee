import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("counter", () => {
  const useMessage = useMessageStore();
  const loginName = ref("");
  const userStore = useUserStore();
  const isLogin = computed(() => {
    return loginName.value !== "";
  });
  const login = (userName: string, password: string): void => {
    if (userStore.login(userName, password)) {
      console.log(userName, password);
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    } else {
      useMessage.showMessage("Login or Password wong");
    }
  };
  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
